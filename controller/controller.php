<?php

    if (isset($_POST['send'])){

        include "../models/calculos.php";
        require "../models/latas.php";
        include "../models/validate.php";
   
        $calculo = new Calculo();
        $validacao = new validate();

        //Coleta das informações no INPUT
     
        //PORTA 1
        !empty($_POST['altura1']) ? $alturaParede1 = $_POST['altura1'] : $alturaParede1 = 0;
        !empty($_POST['largura1']) ? $larguraParede1 = $_POST['largura1'] : $larguraParede1 = 0;

        !empty($_POST['janela1']) ? $qtdjanela1 = $_POST['janela1'] : $qtdJanela1 = 0;
        !empty($_POST['porta1']) ? $qtdPortas1 = $_POST['porta1'] : $qtdPortas1 = 0;

        $resultadoParede1 = $calculo->calculoArea($alturaParede1, $larguraParede1, $qtdjanela1, $qtdPortas1);

        if($qtdJanela1 > 0) 
            $validacao->validateAlturaParede($alturaParede1);

        $validacao->validateAreaParede($resultadoParede1['AreaParede']);
        $validacao->validacaoItensParedes($resultadoParede1['AreaPortaJanela'], $resultadoParede1['AreaParede']);

        //PORTA 2
        !empty($_POST['altura2']) ? $alturaParede2 = $_POST['altura2'] : $alturaParede2 = 0;
        !empty($_POST['largura2']) ? $larguraParede2 = $_POST['largura2'] : $larguraParede2 = 0;

        !empty($_POST['janela2']) ? $qtdJanela2 = $_POST['janela2'] : $qtdJanela2 = 0;
        !empty($_POST['porta2']) ? $qtdPortas2 = $_POST['porta2'] : $qtdPortas2 = 0;

        $resultadoParede2 = $calculo->calculoArea($alturaParede2, $larguraParede2, $qtdJanela2, $qtdPortas2);
        
        if($qtdJanela2 > 0) 
            $validacao->validateAlturaParede($alturaParede2);

        $validacao->validateAreaParede($resultadoParede2['AreaParede']);
        $validacao->validacaoItensParedes($resultadoParede2['AreaPortaJanela'], $resultadoParede2['AreaParede']);

        //PORTA 3
        !empty($_POST['altura3']) ? $alturaParede3 = $_POST['altura3'] : $alturaParede3 = 0;
        !empty($_POST['largura3']) ? $larguraParede3 = $_POST['largura3'] : $larguraParede3 = 0;

        !empty($_POST['janela3']) ? $qtdJanela3 = $_POST['janela3'] : $qtdJanela3 = 0;
        !empty($_POST['porta3']) ? $qtdPortas3 = $_POST['porta3'] : $qtdPortas3 = 0;

        $resultadoParede3 = $calculo->calculoArea($alturaParede3, $larguraParede3, $qtdJanela3, $qtdPortas3);
        
        if($qtdJanela3 > 0) 
            $validacao->validateAlturaParede($alturaParede3);

        $validacao->validateAreaParede($resultadoParede3['AreaParede']);
        $validacao->validacaoItensParedes($resultadoParede3['AreaPortaJanela'], $resultadoParede3['AreaParede']);

        //PORTA 4
        !empty($_POST['altura4']) ? $alturaParede4 = $_POST['altura4'] : $alturaParede4 = 0;
        !empty($_POST['largura4']) ? $larguraParede4 = $_POST['largura4'] : $larguraParede4 = 0;

        !empty($_POST['janela4']) ? $qtdJanela4 = $_POST['janela4'] : $qtdJanela4 = 0;
        !empty($_POST['porta4']) ? $qtdPortas4 = $_POST['porta4'] : $qtdPortas4 = 0;

        $resultadoParede4 = $calculo->calculoArea($alturaParede4, $larguraParede4, $qtdJanela4, $qtdPortas4);
        
        if($qtdJanela4 > 0) 
            $validacao->validateAlturaParede($alturaParede4);

        $validacao->validateAreaParede($resultadoParede4['AreaParede']);
        $validacao->validacaoItensParedes($resultadoParede4['AreaPortaJanela'], $resultadoParede4['AreaParede']);


        $areaPintar = $resultadoParede1['areaPintar'] + $resultadoParede2['areaPintar'] + $resultadoParede3['areaPintar'] + $resultadoParede4['areaPintar'];

        $calculoLata = new Latas();
        $qtdLatas = $calculoLata->calculoLatas($areaPintar);

        header("Location: ../Views/calculo-tintas.php?areaPintar=$areaPintar&lataTinta=$qtdLatas");
    }
?>
