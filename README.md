## Introdução 
Olá! Me chamo João Vitor e esse é minha resposta ao Code Challenge / Digital Republic

## Tecnologias Usadas 
- PHP 
- HTML
- CSS
 
 ## Requisitos
- Apache 
 
 ## Para usar o projeto
- Coloque a pasta do projeto dentro do seu servidor apache
- Vá ate o seu navegador e digite o caminho ate a pasta do projeto.
 
 ## Como usar
- Entrando no sistema digite o tamanho e a largura da parede, também informe a quantidade de janelas e portas ao sistema. 
- Pronto ele vai te informar a area da parede, a quantida de tinta necessaria, e quais lata você vai ter que usar.
