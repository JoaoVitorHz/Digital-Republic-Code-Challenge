<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="../Library/fontawesome/css/all.min.css">
    <title>Tintas</title>
</head>
<body>

    <header>
        <div class="header">
            <div class="div-title">
                <span>Digital Republic / Code Challenge</span>
            </div>
            <div class="div-menu">
                <a href="https://statuesque-crepe-cd6310.netlify.app/" target="_blank"><i class="fa-solid fa-laptop-code"></i>Portfolio</a>
                <a href="https://www.linkedin.com/in/jo%C3%A3o-vitor-araujo-96a78522b/" target="_blank"><i class="fa-brands fa-linkedin"></i>Linkedin</a>
                <a href="https://github.com/JoaoVitorHz" target="_blank"><i class="fa-brands fa-square-github"></i>GitHub</a>
                <a href="../assets/curriculo.pdf" download="Currículo João Vitor" target="_blank"><i class="fa-solid fa-file-arrow-down"></i>Baixar Curriculo</a>
            </div>
        </div>
    </header>

    <section>
        <form method="POST" action="../controller/controller.php">

            <div class="porta porta1">
                <h1>Primeira Parede</h1>

                <div class="div-input-content">
                    <label for="">Altura da Parede:</label>
                    <input type="text" name="altura1" autocomplete="off" placeholder="Digite a altura da parede">
                </div>

                <div class="div-input-content">
                    <label for="">Altura da Parede:</label>
                    <input type="text" name="largura1" autocomplete="off" placeholder="Digite a largura da parede">
                </div>

                <div class="div-input-content">
                    <label for="">Quantidade de portas</label>
                    <input type="text" name="porta1" autocomplete="off" placeholder="Digite a quantida de portas">
                </div>

                <div class="div-input-content">
                    <label for="">Quantidade de Janelas</label>
                    <input type="text" name="janela1" autocomplete="off" placeholder="Digite a quantidade de janelas">
                </div>
               
            </div>
            <div class="porta porta2">
                <h1>Segunda Parede</h1>

                <div class="div-input-content">
                    <label for="">Altura da Parede:</label>
                    <input type="text" name="altura2" autocomplete="off" placeholder="Digite a altura da parede" >
                </div>
                <div class="div-input-content">
                     <label for="">Largura da Parede</label>
                    <input type="text" name="largura2"autocomplete="off"  placeholder="Digite a largura da parede">
                </div>
                <div class="div-input-content">
                    <label for="">Quantidade de portas</label>
                <input type="text" name="porta2" autocomplete="off" placeholder="Digite a quantida de portas">
                </div>
                <div class="div-input-content">
                    <label for="">Quantidade de portas</label>
                    <input type="text" name="janela2" autocomplete="off" placeholder="Digite a quantida de janelas">
                </div>
            </div>
            <div class="porta porta3">
                <h1>Terceira Parede</h1>

                <div class="div-input-content">
                     <label for="">Altura da Parede:</label>
                    <input type="text" name="altura3" autocomplete="off" placeholder="Digite a altura da parede">
                </div>
                <div class="div-input-content">
                    <label for="">Largura da Parede</label>
                    <input type="text" name="largura3" autocomplete="off" placeholder="Digite a largura da parede">
                </div>
                <div class="div-input-content">
                    <label for="">Quantidade de portas</label> 
                    <input type="text" name="porta3" autocomplete="off" placeholder="Digite a quantida de portas">
                </div>
                <div class="div-input-content">
                    <label for="">Quantidade de portas</label>
                    <input type="text" name="janela3" autocomplete="off" placeholder="Digite a quantida de janelas">
                </div>
            </div>
            <div class="porta porta4">
                <h1>Quarta Parede</h1>

                <div class="div-input-content">
                    <label for="">Altura da Parede:</label>
                    <input type="text" name="altura4" autocomplete="off" placeholder="Digite a altura da parede">
                </div>
                <div class="div-input-content">
                    Largura da Parede
                    <input type="text" name="largura4" autocomplete="off" placeholder="Digite a largura da porta">
                </div>
                <div class="div-input-content">
                    <label for="">Quantidade de portas</label>
                <input type="text" name="porta4"  autocomplete="off" placeholder="Digite a quantida de portas">
                </div>
                <div class="div-input-content">
                    <label for="">Quantidade de portas</label>
                <input type="text" name="janela4"  autocomplete="off" placeholder="Digite a quantida de Janelas">
                </div>
            </div>

            <div class="div-info">

                <?php 
                    if(isset($_GET['areaPintar']))
                    echo "<p>Você vai precisar pintar uma aréa de: ".$_GET['areaPintar']."m²!</p>";

                    if(isset($_GET['lataTinta']))
                    echo "<p>Você vai precisar de latas de tinta do seguinte tamanho: ".$_GET['lataTinta']."!</p>";

                    if(isset($_GET['error']))
                    echo '<p class="div-erro">'.$_GET['error'].'</p>';
                ?>
                <button type="submit" name="send">Calcular</button>
            </div>
        </form>
    </section>

</body>
</html>

