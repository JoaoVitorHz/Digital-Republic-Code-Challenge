<?php 

class Calculo{
	public function calculoArea($alturaParede, $larguraParede, $qtdJanela, $qtdPorta)
	{
		//2,40 é a area total de uma janela
		//1,52 é a area total de uma porta

		$areaParede = $alturaParede * $larguraParede;
		$areaJanela = 2.40 * $qtdJanela;
		$areaPorta = 1.52 * $qtdPorta;
		$areaPortaJanela = $areaJanela + $areaPorta;
		$areaPintar = $areaParede - $areaPortaJanela;

		return ['AreaPortaJanela' => $areaPortaJanela, 'AreaParede' => $areaParede, 'areaPintar' => $areaPintar];
	}

}